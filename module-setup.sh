#!/bin/bash
# -*- mode: shell-script; indent-tabs-mode: nil; sh-basic-offset: 4; -*-
# ex: ts=8 sw=4 sts=4 et filetype=sh

check() {
  return 255
}

depends() {
  echo 'network busybox bash'
}

install() {
  inst_hook pre-udev 99 "$moddir/start-ntpd.sh"
  inst_script "$moddir/poll-ntpd.sh" "/bin/poll-ntpd.sh"
  inst_simple "/etc/ntp.conf"
}
