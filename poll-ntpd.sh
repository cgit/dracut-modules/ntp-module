#!/bin/bash

# This script waits for the network to come online and then will set the system time.
#
# This is useful because my system motherboard does not save the system time
# for some reason.

timeout=600
count=0

# Make sure we can resolve DNS addresses
echo "nameserver 8.8.8.8" > /etc/resolv.conf

# Wait for the given interface to come up and for a route to
# be added.
interface="$1"
echo -n "Waiting for interface $interface ... "
while ! ( ip route list dev "$interface" &>/dev/null ) ; do
  if [[ "$count" -eq "$timeout" ]] ; then
    echo "Timedout waiting for $interface. Exiting."
    exit 1
  fi

  echo -n "."
  sleep 0.1
  count=$((count + 1))
done
echo -e "\ndone"

# Even though a route has been added, it still takes time for DNS
# to be come resolvable. Wait till we can ping one of the NTP
# servers.
echo -n "Waiting for 1.gentoo.pool.ntp.org to become reachable ... "
timeout=10
count=0

while ! ( ping -c 1 1.gentoo.pool.ntp.org ) ; do
  if [[ "$count" -eq "$timeout" ]] ; then
    echo "Timedout waiting for 1.gentoo.pool.ntp.org. Exiting."
    exit 1
  fi

  echo -n "."
  sleep 1
  count=$((count + 1))
done
echo -e "\ndone"

# Finally, set the time.
echo -n "Setting time ... "
ntpd -I "$interface" -n -q

if [[ "$?" -ne 0 ]] ; then
  exit 1
fi
echo "done"

# Touch a file to notify other scripts that the time has been set.
touch /var/ntpd-time-set.target
